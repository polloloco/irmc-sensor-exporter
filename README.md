# iRMC S4 Sensor Exporter

[![pipeline status](https://gitlab.com/polloloco/irmc-sensor-exporter/badges/master/pipeline.svg)](https://gitlab.com/polloloco/irmc-sensor-exporter/-/commits/master)

A simple application written in [Rust](https://www.rust-lang.org/) to retrieve sensor data from the remote management interface on some Fujitsu Servers and save them to an InfluxDB database for further processing (Grafana or similar). Currently the tool is able to extract data from the temperature sensors, fan speeds and power usage.

NB: Only tested on iRMC S4. Older/newer versions might work, but I don't have any servers using that version. Feel free to create a pull request if you want to add support.

## Compiling

First you have to [install rust](https://www.rust-lang.org/tools/install) on your system and switch to the nightly toolchain with this command
```bash
rustup default nightly
```
After cloning this repo, run this command to compile it
```bash
cargo build
```

## Usage

After compiling you can use this command to display the help page

```bash
cargo run -- -h
```

Most settings are available as both commandline arguments and environment variables. Environment variables are preferred and will be used for the docker version (Soon™) and are preferred. You can also save all environment variables to a file and the tool will load it.
To do that, copy the file `.env.example` to `.env`
```bash
cp .env.example .env
```
Then, edit the `.env` file and fill in your settings. Any settings saved in this file do not need to be passed as commandline arguments.

To start the program, just type this in your terminal
```bash
cargo run
```

## TODO

Right now everything is in one crate, ideally the part in lib.rs should be its own library and main.rs should be a standalone application using the library.

## Contributing
Pull requests are welcome.

## License
[MIT](https://choosealicense.com/licenses/mit/)