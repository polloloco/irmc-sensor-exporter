FROM debian:buster-slim
COPY irmc-sensor-exporter /irmc-sensor-exporter
CMD ["/irmc-sensor-exporter"]