#[derive(thiserror::Error, Debug)]
pub enum IRMCError {
    #[error("query failed due to unexpected network error: {source}")]
    NetworkError { source: reqwest::Error },
    #[error("invalid credentials")]
    InvalidCredentials,
    #[error("iRMC returned unexpected status code: {status}")]
    UnexpectesStatus { status: reqwest::StatusCode },
    #[error("failed to read response from iRMC")]
    InvalidResponse,
    #[error("failed to parse iRMC response")]
    ParseError {
        #[from]
        from: IRMCParseError,
    },
}

#[derive(thiserror::Error, Debug)]
#[error("failed to parse iRMC response")]
pub struct IRMCParseError;
