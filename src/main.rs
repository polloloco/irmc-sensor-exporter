use influxdb2::models::DataPoint;
use std::net::Ipv4Addr;
use std::time::Duration;
use structopt::StructOpt;

use log::{debug, info, warn};

use irmc_sensor_exporter::*;

#[derive(StructOpt, Debug)]
#[structopt(author)]
struct Opt {
    /// Activate debug mode
    #[structopt(short, long)]
    debug: bool,

    /// Use local demo data (only for testing)
    #[structopt(short, long)]
    testing: bool,

    /// iRMC User
    #[structopt(short, long, env = "IRMC_USERNAME", hide_env_values = true)]
    user: String,

    /// iRMC Password
    #[structopt(short, long, env = "IRMC_PASSWORD", hide_env_values = true)]
    password: String,

    /// iRMC IP-Address
    #[structopt(short, long, env = "IRMC_IP", hide_env_values = true)]
    ip: Ipv4Addr,

    /// InfluxDB host in protocol://server:port format
    /// Example: http://localhost:8888
    #[structopt(long, env = "INFLUXDB_HOST")]
    influx_host: String,

    /// InfluxDB Organization
    #[structopt(long, env = "INFLUXDB_ORG")]
    influx_org: String,

    /// InfluxDB Apikey
    #[structopt(long, env = "INFLUXDB_TOKEN")]
    influx_token: String,

    /// InfluxDB Bucket
    #[structopt(long, env = "INFLUXDB_BUCKET")]
    influx_bucket: String,

    /// Query interval in minutes
    #[structopt(long, env = "QUERY_INTERVAL", default_value = "1")]
    query_interval: u64,

    /// Optionally specify how many queries should be sent.
    /// Program quits when number specified is reached.
    /// Default is no limit
    #[structopt(long, env = "QUERY_MAX_ITERATIONS")]
    max_iterations: Option<u64>,

    /// After how many failed iRMC queries (in a row)
    /// we should quit
    #[structopt(long, env = "MAX_FAILED_QUERIES", default_value = "3")]
    max_failed_queries: u32,

    /// After how many failed InfluxDB writes (in a row)
    /// we should quit
    #[structopt(long, env = "MAX_FAILED_WRITES", default_value = "3")]
    max_failed_writes: u32,
}

#[derive(thiserror::Error, Debug)]
enum Error {
    #[error(transparent)]
    IRMCError(#[from] error::IRMCError),
    #[error(transparent)]
    InfluxWriteError(#[from] InfluxWriteError),
}

#[derive(thiserror::Error, Debug)]
#[error("failed to write data to InfluxDB: {source}")]
struct InfluxWriteError {
    #[from]
    source: influxdb2::RequestError,
}

type Result<T, E = Error> = std::result::Result<T, E>;

fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    dotenv::dotenv().ok();
    let opt = Opt::from_args();
    if opt.debug {
        simple_logger::init_with_level(log::Level::Debug).unwrap();
    } else {
        simple_logger::init_with_level(log::Level::Info).unwrap();
    }

    debug!("Starting with config: {:#?}", opt);

    tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .unwrap()
        .block_on(async move { async_main(opt).await })
}

async fn async_main(opt: Opt) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    if opt.testing {
        let data = parse_response(DEMO_XML)?;
        info!("{:#?}", data);
        return Ok(());
    }

    let max_iterations = opt.max_iterations;
    let query_interval = opt.query_interval;

    let mut s: S = opt.into();

    let mut counter = 0;
    if let Some(max) = max_iterations {
        info!("Quitting after {} queries", max);
    }

    let mut interval = tokio::time::interval(Duration::from_secs(query_interval * 60));
    loop {
        interval.tick().await;

        s.run().await?;

        counter += 1;
        if let Some(max) = max_iterations {
            if counter == max {
                break;
            }
        }
    }

    Ok(())
}

struct S {
    irmc_username: String,
    irmc_password: String,
    irmc_ip: Ipv4Addr,
    influx_client: influxdb2::Client,
    influx_bucket: String,
    failed_queries: u32,
    max_failed_queries: u32,
    failed_writes: u32,
    max_failed_writes: u32,
}

impl S {
    pub fn new(
        irmc_username: String,
        irmc_password: String,
        irmc_ip: Ipv4Addr,
        influx_host: String,
        influx_org: String,
        influx_token: String,
        influx_bucket: String,
        max_failed_queries: u32,
        max_failed_writes: u32,
    ) -> Self {
        let influx_client = influxdb2::Client::new(&influx_host, &influx_org, &influx_token);
        Self {
            irmc_username,
            irmc_password,
            irmc_ip,
            influx_client,
            influx_bucket,
            failed_queries: 0,
            max_failed_queries,
            failed_writes: 0,
            max_failed_writes,
        }
    }

    pub async fn run(&mut self) -> Result<()> {
        info!("--> Sending query to iRMC");
        match self.query_server().await {
            Ok(data) => {
                self.failed_queries = 0;
                info!("<-- Received response from iRMC");

                let points = Self::create_points_from_data(data);

                self.write_data(points).await?;
            }
            Err(err) => {
                self.failed_queries += 1;
                warn!(
                    "<-- Failed to read response from iRMC: {} (failed queries in a row: {})",
                    err, self.failed_queries
                );

                if self.failed_queries >= self.max_failed_queries {
                    warn!(
                        "iRMC query failed after {} retries, giving up...",
                        self.failed_queries
                    );
                    return Err(err);
                }
            }
        }
        Ok(())
    }

    async fn query_server(&mut self) -> Result<SensorData> {
        Ok(query_server(self.irmc_ip, &self.irmc_username, &self.irmc_password).await?)
    }

    async fn write_data(&mut self, points: Vec<DataPoint>) -> Result<(), InfluxWriteError> {
        info!("--> Writing data to InfluxDB");
        match self
            .influx_client
            .write(&self.influx_bucket, futures::prelude::stream::iter(points))
            .await
        {
            Ok(()) => {
                self.failed_writes = 0;
                info!("<-- Data saved to InfluxDB");
            }
            Err(err) => {
                self.failed_writes += 1;
                warn!(
                    "<-- Failed to send data to InfluxDB: {} (failed writes in a row: {})",
                    err, self.failed_writes
                );

                if self.failed_writes >= self.max_failed_writes {
                    warn!(
                        "Saving to InfluxDB failed after {} retries, giving up...",
                        self.failed_writes
                    );
                    return Err(err.into());
                }
            }
        }
        Ok(())
    }

    fn create_points_from_data(data: SensorData) -> Vec<DataPoint> {
        let mut points = vec![];
        for temperature in data.temperatures.into_iter() {
            let value = temperature.value.unwrap_or(0);
            //info!("Pushing temperature \"{}\" => {}", temperature.name, value);
            points.push(
                DataPoint::builder("temperature")
                    .tag("type", temperature.name)
                    .tag("status", temperature.status.description)
                    .field("value", value as i64)
                    .timestamp(data.timestamp.timestamp_nanos())
                    .build()
                    .unwrap(),
            );
        }
        for fan in data.fans.into_iter() {
            //info!("Pushing fan speed \"{}\" => {}", fan.name, fan.curr_speed);
            points.push(
                DataPoint::builder("fan")
                    .tag("type", fan.name)
                    .tag("status", fan.status.description)
                    .field("value", fan.curr_speed as i64)
                    .timestamp(data.timestamp.timestamp_nanos())
                    .build()
                    .unwrap(),
            );
        }
        points.push(
            DataPoint::builder("power")
                .field("value", data.power_load as i64)
                .timestamp(data.timestamp.timestamp_nanos())
                .build()
                .unwrap(),
        );
        points
    }
}

impl std::convert::From<Opt> for S {
    fn from(from: Opt) -> Self {
        Self::new(
            from.user,
            from.password,
            from.ip,
            from.influx_host,
            from.influx_org,
            from.influx_token,
            from.influx_bucket,
            from.max_failed_queries,
            from.max_failed_writes,
        )
    }
}

const DEMO_XML: &str = r#"<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="Report.xslt"?>
<Root Schema="2" Version="9.62F" OS="iRMC S4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
 <Summary>
  <Created>
   <IsAdmin>true</IsAdmin>
   <Date>2021/08/30 12:46:14</Date>
   <BuildDuration>29</BuildDuration>
   <Computer>iRMCXXXXXX</Computer>
   <Company>FUJITSU</Company>
   <OS>iRMC S4 9.62F SDR: 3.62   ID 0355  RX200S8</OS>
   <Domain></Domain>
   <HostIPv4Address>10.10.20.5</HostIPv4Address>
  </Created>
  <Errors Count="2">
   <SystemEventlog>
    <Message>1 important error(s) in system event log (SEL)!</Message>
   </SystemEventlog>
   <InternalEventlog>
    <Message>1 important error(s) in internal event log (IEL)!</Message>
   </InternalEventlog>
  </Errors>
  <Warnings Count="2">
   <SystemEventlog>
    <Message>4 important warning(s) in system event log SEL</Message>
   </SystemEventlog>
   <InternalEventlog>
    <Message>39 important warning(s) in internal event log IEL</Message>
   </InternalEventlog>
  </Warnings>
  <Content>
   <Item Name="System/AISConnect"></Item>
   <Item Name="System/BIOS"></Item>
   <Item Name="System/Processor"></Item>
   <Item Name="System/Memory"></Item>
   <Item Name="System/Fans"></Item>
   <Item Name="System/Temperatures"></Item>
   <Item Name="System/PowerSupplies"></Item>
   <Item Name="System/Voltages"></Item>
   <Item Name="System/IDPROMS"></Item>
   <Item Name="System/SensorDataRecords"></Item>
   <Item Name="System/PCIDevices"></Item>
   <Item Name="System/SystemEventlog"></Item>
   <Item Name="System/InternalEventlog"></Item>
   <Item Name="System/BootStatus"></Item>
   <Item Name="System/ManagementControllers"></Item>
   <Item Name="Network/Settings"></Item>
   <Item Name="Network/Adapters"></Item>
   <Item Name="Network/Interfaces"></Item>
   <Item Name="Network/Ports"></Item>
   <Item Name="Software/ServerView/SNMPAgents"></Item>
   <Item Name="Software/ServerView/ServerViewRaid"></Item>
  </Content>
 </Summary>
 <System>
  <Fans Schema="1" Count="14">
   <Fan Name="FAN1 SYS" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>10200</CurrSpeed>
    <CurrMaxSpeed>6720</CurrMaxSpeed>
    <NomMaxSpeed>6840</NomMaxSpeed>
   </Fan>
   <Fan Name="FAN2 SYS" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>11400</CurrSpeed>
    <CurrMaxSpeed>7440</CurrMaxSpeed>
    <NomMaxSpeed>7440</NomMaxSpeed>
   </Fan>
   <Fan Name="FAN3 SYS" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>10320</CurrSpeed>
    <CurrMaxSpeed>6960</CurrMaxSpeed>
    <NomMaxSpeed>6840</NomMaxSpeed>
   </Fan>
   <Fan Name="FAN4 SYS" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>11400</CurrSpeed>
    <CurrMaxSpeed>7440</CurrMaxSpeed>
    <NomMaxSpeed>7440</NomMaxSpeed>
   </Fan>
   <Fan Name="FAN5 SYS" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>10200</CurrSpeed>
    <CurrMaxSpeed>6840</CurrMaxSpeed>
    <NomMaxSpeed>6840</NomMaxSpeed>
   </Fan>
   <Fan Name="FAN6 SYS" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>11400</CurrSpeed>
    <CurrMaxSpeed>7440</CurrMaxSpeed>
    <NomMaxSpeed>7440</NomMaxSpeed>
   </Fan>
   <Fan Name="FAN7 SYS" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>10200</CurrSpeed>
   </Fan>
   <Fan Name="FAN8 SYS" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>11400</CurrSpeed>
   </Fan>
   <Fan Name="FAN9 SYS" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>10200</CurrSpeed>
   </Fan>
   <Fan Name="FAN10 SYS" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>11400</CurrSpeed>
   </Fan>
   <Fan Name="FAN11 SYS" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>10200</CurrSpeed>
    <CurrMaxSpeed>6720</CurrMaxSpeed>
    <NomMaxSpeed>6720</NomMaxSpeed>
   </Fan>
   <Fan Name="FAN12 SYS" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>11400</CurrSpeed>
    <CurrMaxSpeed>7320</CurrMaxSpeed>
    <NomMaxSpeed>7440</NomMaxSpeed>
   </Fan>
   <Fan Name="FAN PSU1" CSS="true">
    <Status Description="redundant-fail">4</Status>
    <CurrSpeed>0</CurrSpeed>
    <CurrMaxSpeed>17920</CurrMaxSpeed>
    <NomMaxSpeed>17920</NomMaxSpeed>
   </Fan>
   <Fan Name="FAN PSU2" CSS="true">
    <Status Description="ok">1</Status>
    <CurrSpeed>4000</CurrSpeed>
    <CurrMaxSpeed>17920</CurrMaxSpeed>
    <NomMaxSpeed>17920</NomMaxSpeed>
   </Fan>
  </Fans>
  <Temperatures Schema="1" Count="24">
   <Temperature Name="Ambient" CSS="false">
    <Status Description="ok">6</Status>
    <CurrValue>30</CurrValue>
    <WarningThreshold>37</WarningThreshold>
    <CriticalThreshold>42</CriticalThreshold>
   </Temperature>
   <Temperature Name="Systemboard" CSS="false">
    <Status Description="ok">6</Status>
    <CurrValue>43</CurrValue>
    <WarningThreshold>75</WarningThreshold>
    <CriticalThreshold>80</CriticalThreshold>
   </Temperature>
   <Temperature Name="VR CPU1" CSS="false">
    <Status Description="ok">6</Status>
    <CurrValue>46</CurrValue>
    <WarningThreshold>120</WarningThreshold>
    <CriticalThreshold>125</CriticalThreshold>
   </Temperature>
   <Temperature Name="VR MEM AB" CSS="false">
    <Status Description="ok">6</Status>
    <CurrValue>36</CurrValue>
    <WarningThreshold>120</WarningThreshold>
    <CriticalThreshold>125</CriticalThreshold>
   </Temperature>
   <Temperature Name="VR MEM CD" CSS="false">
    <Status Description="ok">6</Status>
    <CurrValue>42</CurrValue>
    <WarningThreshold>120</WarningThreshold>
    <CriticalThreshold>125</CriticalThreshold>
   </Temperature>
   <Temperature Name="VR CPU2" CSS="false">
    <Status Description="ok">6</Status>
    <CurrValue>41</CurrValue>
    <WarningThreshold>120</WarningThreshold>
    <CriticalThreshold>125</CriticalThreshold>
   </Temperature>
   <Temperature Name="VR MEM EF" CSS="false">
    <Status Description="ok">6</Status>
    <CurrValue>42</CurrValue>
    <WarningThreshold>120</WarningThreshold>
    <CriticalThreshold>125</CriticalThreshold>
   </Temperature>
   <Temperature Name="VR MEM GH" CSS="false">
    <Status Description="ok">6</Status>
    <CurrValue>36</CurrValue>
    <WarningThreshold>120</WarningThreshold>
    <CriticalThreshold>125</CriticalThreshold>
   </Temperature>
   <Temperature Name="CPU1" CSS="false">
    <Status Description="ok">6</Status>
    <CurrValue>42</CurrValue>
    <WarningThreshold>98</WarningThreshold>
    <CriticalThreshold>99</CriticalThreshold>
   </Temperature>
   <Temperature Name="CPU2" CSS="false">
    <Status Description="ok">6</Status>
    <CurrValue>43</CurrValue>
    <WarningThreshold>98</WarningThreshold>
    <CriticalThreshold>99</CriticalThreshold>
   </Temperature>
   <Temperature Name="MEM A" CSS="true">
    <Status Description="ok">6</Status>
    <CurrValue>38</CurrValue>
    <WarningThreshold>78</WarningThreshold>
    <CriticalThreshold>82</CriticalThreshold>
   </Temperature>
   <Temperature Name="MEM B" CSS="true">
    <Status Description="not available">0</Status>
   </Temperature>
   <Temperature Name="MEM C" CSS="true">
    <Status Description="ok">6</Status>
    <CurrValue>42</CurrValue>
    <WarningThreshold>78</WarningThreshold>
    <CriticalThreshold>82</CriticalThreshold>
   </Temperature>
   <Temperature Name="MEM D" CSS="true">
    <Status Description="not available">0</Status>
   </Temperature>
   <Temperature Name="MEM E" CSS="true">
    <Status Description="ok">6</Status>
    <CurrValue>40</CurrValue>
    <WarningThreshold>78</WarningThreshold>
    <CriticalThreshold>82</CriticalThreshold>
   </Temperature>
   <Temperature Name="MEM F" CSS="true">
    <Status Description="not available">0</Status>
   </Temperature>
   <Temperature Name="MEM G" CSS="true">
    <Status Description="ok">6</Status>
    <CurrValue>37</CurrValue>
    <WarningThreshold>78</WarningThreshold>
    <CriticalThreshold>82</CriticalThreshold>
   </Temperature>
   <Temperature Name="MEM H" CSS="true">
    <Status Description="not available">0</Status>
   </Temperature>
   <Temperature Name="PSU1 Inlet" CSS="true">
    <Status Description="ok">6</Status>
    <CurrValue>38</CurrValue>
    <WarningThreshold>57</WarningThreshold>
    <CriticalThreshold>61</CriticalThreshold>
   </Temperature>
   <Temperature Name="PSU2 Inlet" CSS="true">
    <Status Description="ok">6</Status>
    <CurrValue>39</CurrValue>
    <WarningThreshold>57</WarningThreshold>
    <CriticalThreshold>61</CriticalThreshold>
   </Temperature>
   <Temperature Name="PSU1" CSS="true">
    <Status Description="ok">6</Status>
    <CurrValue>38</CurrValue>
    <WarningThreshold>102</WarningThreshold>
    <CriticalThreshold>107</CriticalThreshold>
   </Temperature>
   <Temperature Name="PSU2" CSS="true">
    <Status Description="ok">6</Status>
    <CurrValue>59</CurrValue>
    <WarningThreshold>102</WarningThreshold>
    <CriticalThreshold>107</CriticalThreshold>
   </Temperature>
   <Temperature Name="BBU" CSS="true">
    <Status Description="not available">0</Status>
   </Temperature>
   <Temperature Name="RAID Controller" CSS="true">
    <Status Description="not available">0</Status>
   </Temperature>
  </Temperatures>
  <PowerSupplies Schema="1" Count="2">
  <PowerSupply Name="PSU1" CSS="true">
   <Status Description="ok">1</Status>
   <Load>80</Load>
  </PowerSupply>
  <PowerSupply Name="PSU2" CSS="true">
   <Status Description="ok">1</Status>
   <Load>76</Load>
  </PowerSupply>
 </PowerSupplies>
</System>
</Root>"#;
