use chrono::{DateTime, Utc};
use log::debug;
use reqwest::ClientBuilder;
use std::net::Ipv4Addr;
use std::time::{Duration, Instant};

use quick_xml::de::from_str;
use serde::{de::Error, Deserialize, Deserializer};

pub mod error;
use error::{IRMCError, IRMCParseError};

static APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"),);

// PUBLIC API

pub async fn query_server(
    host: Ipv4Addr,
    user: &str,
    password: &str,
) -> Result<SensorData, IRMCError> {
    let xml = report_xml_fetch(host, user, password).await?;
    let data = parse_response(&xml)?;
    Ok(data)
}

pub fn parse_response(xml: &str) -> Result<SensorData, IRMCParseError> {
    let raw = report_xml_parse(xml)?;
    let data = report_xml_finalize(raw);
    Ok(data)
}

#[derive(Debug)]
pub struct SensorData {
    pub timestamp: DateTime<Utc>,
    pub host: String,
    pub temperatures: Vec<Temperature>,
    pub fans: Vec<Fan>,
    pub power_load: u16,
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct Temperature {
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "Status")]
    pub status: Status,
    #[serde(rename = "CurrValue")]
    pub value: Option<u8>,
    #[serde(rename = "WarningThreshold")]
    pub warning: Option<u8>,
    #[serde(rename = "CriticalThreshold")]
    pub critical: Option<u8>,
}

impl Temperature {
    pub fn is_ok(&self) -> bool {
        self.status.is_ok()
    }
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct Fan {
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "Status")]
    pub status: Status,
    #[serde(rename = "CurrSpeed")]
    pub curr_speed: u16,
    #[serde(rename = "CurrMaxSpeed")]
    pub curr_max_speed: Option<u16>,
    #[serde(rename = "NomMaxSpeed")]
    pub nom_max_speed: Option<u16>,
}

impl Fan {
    pub fn is_ok(&self) -> bool {
        self.status.is_ok()
    }
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct Status {
    #[serde(rename = "$value")]
    pub value: u8,
    #[serde(rename = "Description")]
    pub description: String,
}

impl Status {
    pub fn is_ok(&self) -> bool {
        self.description == "ok"
    }
}

// PRIVATE API

// Raw structs, only used for parsing

#[derive(Debug, Deserialize, PartialEq)]
struct RawSensorData {
    #[serde(rename = "Summary")]
    summary: RawSummary,
    #[serde(rename = "System")]
    system: RawSystem,
}

impl RawSensorData {
    pub fn finalize(self) -> SensorData {
        let power_load = self
            .system
            .powersupplies
            .values
            .into_iter()
            .filter(|psu| psu.is_ok())
            .map(|psu| psu.load)
            .sum();
        SensorData {
            host: self.summary.created.host,
            timestamp: self.summary.created.timestamp,
            temperatures: self.system.temperatures.values,
            fans: self.system.fans.values,
            power_load,
        }
    }
}

#[derive(Debug, Deserialize, PartialEq)]
struct RawSummary {
    #[serde(rename = "Created")]
    created: RawCreated,
}

#[derive(Debug, Deserialize, PartialEq)]
struct RawCreated {
    #[serde(rename = "Date", deserialize_with = "timestamp_deserialize")]
    timestamp: DateTime<Utc>,
    #[serde(rename = "Computer")]
    host: String,
}

fn timestamp_deserialize<'de, D: Deserializer<'de>>(
    deserializer: D,
) -> Result<DateTime<Utc>, D::Error> {
    let time: String = Deserialize::deserialize(deserializer)?;
    let naivedatetime = chrono::NaiveDateTime::parse_from_str(&time, "%Y/%m/%d %H:%M:%S")
        .map_err(D::Error::custom)?;
    let result = DateTime::<Utc>::from_utc(naivedatetime, Utc);
    Ok(result)
}

#[derive(Debug, Deserialize, PartialEq)]
struct RawSystem {
    #[serde(rename = "Temperatures")]
    pub temperatures: RawTemperatures,
    #[serde(rename = "Fans")]
    pub fans: RawFans,
    #[serde(rename = "PowerSupplies")]
    pub powersupplies: RawPowerSupplies,
}

#[derive(Debug, Deserialize, PartialEq)]
struct RawTemperatures {
    #[serde(rename = "Temperature")]
    pub values: Vec<Temperature>,
}

#[derive(Debug, Deserialize, PartialEq)]
struct RawFans {
    #[serde(rename = "Fan")]
    pub values: Vec<Fan>,
}

#[derive(Debug, Deserialize, PartialEq)]
struct RawPowerSupplies {
    #[serde(rename = "PowerSupply")]
    pub values: Vec<RawPowerSupply>,
}

#[derive(Debug, Deserialize, PartialEq)]
struct RawPowerSupply {
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "Status")]
    pub status: Status,
    #[serde(rename = "Load")]
    pub load: u16,
}

impl RawPowerSupply {
    pub fn is_ok(&self) -> bool {
        self.status.is_ok()
    }
}

async fn report_xml_fetch(host: Ipv4Addr, user: &str, password: &str) -> Result<String, IRMCError> {
    let client = ClientBuilder::new()
        .user_agent(APP_USER_AGENT)
        .timeout(Duration::from_secs(60))
        .connect_timeout(Duration::from_secs(5))
        .build()
        .expect("Failed to build http Client");

    let url = format!("http://{}/report.xml", host);

    let start = Instant::now();

    let response = client
        .get(&url)
        .basic_auth(user, Some(password))
        .send()
        .await
        .map_err(|err| IRMCError::NetworkError { source: err })?;

    let end = Instant::now();
    debug!("Request to {} took {}ms", url, (end - start).as_millis());

    let status = response.status();
    if status == 401 {
        return Err(IRMCError::InvalidCredentials);
    } else if status != 200 {
        return Err(IRMCError::UnexpectesStatus { status });
    }

    let response_text = response
        .text()
        .await
        .map_err(|_| IRMCError::InvalidResponse)?;

    Ok(response_text)
}

fn report_xml_parse(xml: &str) -> Result<RawSensorData, IRMCParseError> {
    debug!("Called parse for xml:\n{}", xml);
    let data: RawSensorData = from_str(xml).map_err(|_| IRMCParseError)?;
    Ok(data)
}

fn report_xml_finalize(raw_data: RawSensorData) -> SensorData {
    raw_data.finalize()
}

#[cfg(test)]
mod tests {
    #[test]
    fn dual_psu_both_working() {
        const XML: &str = r#"
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="Report.xslt"?>
<Root Schema="2" Version="9.62F" OS="iRMC S4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <System>
        <PowerSupplies Schema="1" Count="2">
            <PowerSupply Name="PSU1" CSS="true">
                <Status Description="ok">1</Status>
                <Load>100</Load>
            </PowerSupply>
            <PowerSupply Name="PSU2" CSS="true">
                <Status Description="ok">1</Status>
                <Load>90</Load>
            </PowerSupply>
        </PowerSupplies>
        <Temperatures Schema="1" Count="24">
            <Temperature Name="Ambient" CSS="false">
                <Status Description="ok">6</Status>
                <CurrValue>30</CurrValue>
                <WarningThreshold>37</WarningThreshold>
                <CriticalThreshold>42</CriticalThreshold>
            </Temperature>
        </Temperatures>
        <Fans Schema="1" Count="1">
            <Fan Name="FAN1 SYS" CSS="true">
                <Status Description="ok">1</Status>
                <CurrSpeed>10200</CurrSpeed>
                <CurrMaxSpeed>6720</CurrMaxSpeed>
                <NomMaxSpeed>6840</NomMaxSpeed>
            </Fan>
        </Fans>
    </System>
</Root>
"#;
        use super::*;
        let data = report_xml_parse(XML).unwrap();
        let data = report_xml_finalize(data);
        assert_eq!(data.power_load, 190);
    }

    #[test]
    fn dual_psu_one_failed() {
        const XML: &str = r#"
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="Report.xslt"?>
<Root Schema="2" Version="9.62F" OS="iRMC S4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <System>
        <PowerSupplies Schema="1" Count="2">
            <PowerSupply Name="PSU1" CSS="true">
                <Status Description="AC fail">3</Status>
                <Load>150</Load>
            </PowerSupply>
            <PowerSupply Name="PSU2" CSS="true">
                <Status Description="ok">1</Status>
                <Load>150</Load>
            </PowerSupply>
        </PowerSupplies>
        <Temperatures Schema="1" Count="24">
            <Temperature Name="Ambient" CSS="false">
                <Status Description="ok">6</Status>
                <CurrValue>30</CurrValue>
                <WarningThreshold>37</WarningThreshold>
                <CriticalThreshold>42</CriticalThreshold>
            </Temperature>
        </Temperatures>
        <Fans Schema="1" Count="1">
            <Fan Name="FAN1 SYS" CSS="true">
                <Status Description="ok">1</Status>
                <CurrSpeed>10200</CurrSpeed>
                <CurrMaxSpeed>6720</CurrMaxSpeed>
                <NomMaxSpeed>6840</NomMaxSpeed>
            </Fan>
        </Fans>
    </System>
</Root>
"#;
        use super::*;
        let data = report_xml_parse(XML).unwrap();
        let data = report_xml_finalize(data);
        assert_eq!(data.power_load, 150);
    }

    #[test]
    fn temperatures() {
        const XML: &str = r#"
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="Report.xslt"?>
<Root Schema="2" Version="9.62F" OS="iRMC S4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <System>
        <PowerSupplies Schema="1" Count="2">
            <PowerSupply Name="PSU1" CSS="true">
                <Status Description="AC fail">3</Status>
                <Load>150</Load>
            </PowerSupply>
            <PowerSupply Name="PSU2" CSS="true">
                <Status Description="ok">1</Status>
                <Load>150</Load>
            </PowerSupply>
        </PowerSupplies>
        <Temperatures Schema="1" Count="24">
            <Temperature Name="Ambient" CSS="false">
                <Status Description="ok">6</Status>
                <CurrValue>30</CurrValue>
                <WarningThreshold>37</WarningThreshold>
                <CriticalThreshold>42</CriticalThreshold>
            </Temperature>
            <Temperature Name="Systemboard" CSS="false">
                <Status Description="ok">6</Status>
                <CurrValue>43</CurrValue>
                <WarningThreshold>75</WarningThreshold>
                <CriticalThreshold>80</CriticalThreshold>
            </Temperature>
            <Temperature Name="VR CPU1" CSS="false">
                <Status Description="ok">6</Status>
                <CurrValue>46</CurrValue>
                <WarningThreshold>120</WarningThreshold>
                <CriticalThreshold>125</CriticalThreshold>
            </Temperature>
            <Temperature Name="VR MEM AB" CSS="false">
                <Status Description="ok">6</Status>
                <CurrValue>36</CurrValue>
                <WarningThreshold>120</WarningThreshold>
                <CriticalThreshold>125</CriticalThreshold>
            </Temperature>
            <Temperature Name="VR MEM CD" CSS="false">
                <Status Description="ok">6</Status>
                <CurrValue>42</CurrValue>
                <WarningThreshold>120</WarningThreshold>
                <CriticalThreshold>125</CriticalThreshold>
            </Temperature>
            <Temperature Name="VR CPU2" CSS="false">
                <Status Description="ok">6</Status>
                <CurrValue>41</CurrValue>
                <WarningThreshold>120</WarningThreshold>
                <CriticalThreshold>125</CriticalThreshold>
            </Temperature>
            <Temperature Name="VR MEM EF" CSS="false">
                <Status Description="ok">6</Status>
                <CurrValue>42</CurrValue>
                <WarningThreshold>120</WarningThreshold>
                <CriticalThreshold>125</CriticalThreshold>
            </Temperature>
            <Temperature Name="VR MEM GH" CSS="false">
                <Status Description="ok">6</Status>
                <CurrValue>36</CurrValue>
                <WarningThreshold>120</WarningThreshold>
                <CriticalThreshold>125</CriticalThreshold>
            </Temperature>
            <Temperature Name="CPU1" CSS="false">
                <Status Description="ok">6</Status>
                <CurrValue>42</CurrValue>
                <WarningThreshold>98</WarningThreshold>
                <CriticalThreshold>99</CriticalThreshold>
            </Temperature>
            <Temperature Name="CPU2" CSS="false">
                <Status Description="ok">6</Status>
                <CurrValue>43</CurrValue>
                <WarningThreshold>98</WarningThreshold>
                <CriticalThreshold>99</CriticalThreshold>
            </Temperature>
            <Temperature Name="MEM A" CSS="true">
                <Status Description="ok">6</Status>
                <CurrValue>38</CurrValue>
                <WarningThreshold>78</WarningThreshold>
                <CriticalThreshold>82</CriticalThreshold>
            </Temperature>
            <Temperature Name="MEM B" CSS="true">
                <Status Description="not available">0</Status>
            </Temperature>
            <Temperature Name="MEM C" CSS="true">
                <Status Description="ok">6</Status>
                <CurrValue>42</CurrValue>
                <WarningThreshold>78</WarningThreshold>
                <CriticalThreshold>82</CriticalThreshold>
            </Temperature>
            <Temperature Name="MEM D" CSS="true">
                <Status Description="not available">0</Status>
            </Temperature>
            <Temperature Name="MEM E" CSS="true">
                <Status Description="ok">6</Status>
                <CurrValue>40</CurrValue>
                <WarningThreshold>78</WarningThreshold>
                <CriticalThreshold>82</CriticalThreshold>
            </Temperature>
            <Temperature Name="MEM F" CSS="true">
                <Status Description="not available">0</Status>
            </Temperature>
            <Temperature Name="MEM G" CSS="true">
                <Status Description="ok">6</Status>
                <CurrValue>37</CurrValue>
                <WarningThreshold>78</WarningThreshold>
                <CriticalThreshold>82</CriticalThreshold>
            </Temperature>
            <Temperature Name="MEM H" CSS="true">
                <Status Description="not available">0</Status>
            </Temperature>
            <Temperature Name="PSU1 Inlet" CSS="true">
                <Status Description="ok">6</Status>
                <CurrValue>38</CurrValue>
                <WarningThreshold>57</WarningThreshold>
                <CriticalThreshold>61</CriticalThreshold>
            </Temperature>
            <Temperature Name="PSU2 Inlet" CSS="true">
                <Status Description="ok">6</Status>
                <CurrValue>39</CurrValue>
                <WarningThreshold>57</WarningThreshold>
                <CriticalThreshold>61</CriticalThreshold>
            </Temperature>
            <Temperature Name="PSU1" CSS="true">
                <Status Description="ok">6</Status>
                <CurrValue>38</CurrValue>
                <WarningThreshold>102</WarningThreshold>
                <CriticalThreshold>107</CriticalThreshold>
            </Temperature>
            <Temperature Name="PSU2" CSS="true">
                <Status Description="ok">6</Status>
                <CurrValue>59</CurrValue>
                <WarningThreshold>102</WarningThreshold>
                <CriticalThreshold>107</CriticalThreshold>
            </Temperature>
            <Temperature Name="BBU" CSS="true">
                <Status Description="not available">0</Status>
            </Temperature>
            <Temperature Name="RAID Controller" CSS="true">
                <Status Description="not available">0</Status>
            </Temperature>
        </Temperatures>
        <Fans Schema="1" Count="1">
            <Fan Name="FAN1 SYS" CSS="true">
                <Status Description="ok">1</Status>
                <CurrSpeed>10200</CurrSpeed>
                <CurrMaxSpeed>6720</CurrMaxSpeed>
                <NomMaxSpeed>6840</NomMaxSpeed>
            </Fan>
        </Fans>
    </System>
</Root>
"#;

        let expected_temps = [
            Temperature {
                name: "Ambient".to_string(),
                status: Status {
                    description: "ok".to_string(),
                    value: 6,
                },
                value: Some(30),
                warning: Some(37),
                critical: Some(42),
            },
            Temperature {
                name: "Systemboard".to_string(),
                status: Status {
                    description: "ok".to_string(),
                    value: 6,
                },
                value: Some(43),
                warning: Some(75),
                critical: Some(80),
            },
        ];

        use super::*;
        let data = report_xml_parse(XML).unwrap();
        let data = report_xml_finalize(data);

        for (expected, parsed) in expected_temps.iter().zip(data.temperatures.iter()) {
            assert_eq!(expected, parsed);
        }
    }
}
